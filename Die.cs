﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON.LV2
{

    //Zadatak 1.//
    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = new Random();
    //    }
    //    public int Roll()
    //    {
    //        int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }

    //}
    ///////////////////////////////////////


    //Zadatak 2.//
    //class Die
    //{
    //    private int numberOfSides;
    //    private Random randomGenerator;
    //    public Die(int numberOfSides, ref Random ReferenceOnRandomGenerator)
    //    {
    //        this.numberOfSides = numberOfSides;
    //        this.randomGenerator = ReferenceOnRandomGenerator;
    //    }
    //    public int Roll()
    //    {
    //        int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
    //        return rolledNumber;
    //    }

    //}
    ///////////////////////////////


    //Zadatak 3. , 4.//
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }

    }
    //////////////////


}

